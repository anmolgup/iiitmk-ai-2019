import requests
from bs4 import BeautifulSoup
import os
from datetime import datetime
from tqdm import tqdm

marks = {frozenset(["assignment_3", "assignment_4", "assignment_5"]): 2}
headers = {
    "Host": "www.hackerrank.com",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate, br",
    "DNT": "1",
    "Connection": "keep-alive",
    "Upgrade-Insecure-Requests": "1",
    "Pragma": "no-cache",
    "Cache-Control": "no-cache",
}
report = []


for person in tqdm(os.listdir("../people")):
    data = {"name": person, "assignments": set(), "assignment_scores": {}}
    for file in os.listdir(f"../people/{person}"):
        if "readme" in file.lower():
            with open(f"../people/{person}/{file}", "r") as fl:
                lines = list(fl.readlines())
            email = [i for i in lines if "@" in i]
            data["email"] = None if len(email) == 0 else email[0].strip()
            hr = [i for i in lines if "hackerrank" in i]
            hr = None if len(hr) == 0 else hr[0]
            hr = hr[hr.index("https://") :].strip() if hr is not None else None
            data["hackerrank"] = hr
        elif file.endswith(".py"):
            with open(f"../people/{person}/{file}", "r") as fl:
                program = fl.read()
            gl = {}
            exec(program, gl)
            attempts = [i for i in gl.keys() if i.startswith("assignment")]
            data["assignments"] |= set(attempts)
    # ======================
    data["assignment_scores"].update(
        {k: v for k, v in marks.items() if k in data["assignments"]}
    )
    data["marks"] = {"Assignment": sum(data["assignment_scores"].values())}
    data["hackerrank_badges"] = {}
    if data["hackerrank"] is not None:
        r = requests.get(data["hackerrank"], headers=headers)
        soup = BeautifulSoup(r.text, "lxml")
        badges = soup.findAll("svg", {"class": "hexagon"})
        for badge in badges:
            bname = list(badge.findAll("text", {"class": "badge-title"}))[0].text
            if bname == "Python" or bname == "Problem Solving":
                stars = len(badge.findAll("svg", {"class": "badge-star"}))
                data["hackerrank_badges"][bname] = stars
    data["marks"]["HackerRank"] = sum(
        stars * 2 for stars in data["hackerrank_badges"].values()
    )
    report.append(data)

report_text = f"""
Generated on {datetime.utcnow()}

"""

cols = ["name", "HackerRank", "Assignment", "email", "hackerrank link"]
col_sizes = {k: len(k) for k in cols}
for person in report:
    col_sizes["name"] = max(len(person["name"]), col_sizes["name"])
    col_sizes["HackerRank"] = max(
        col_sizes["HackerRank"], len(str(person["marks"]["HackerRank"]))
    )
    col_sizes["Assignment"] = max(
        col_sizes["Assignment"], len(str(person["marks"]["Assignment"]))
    )
    col_sizes["email"] = max(col_sizes["email"], len(str(person["email"])))
    col_sizes["hackerrank link"] = max(
        col_sizes["hackerrank link"], len(str(person["hackerrank"]))
    )
formatted_cols = [(c + " " * 100)[: col_sizes[c]] for c in cols]
report_text += "| " + " | ".join(formatted_cols) + "|\n"
report_text += (
    "| "
    + " | ".join(("".join("-" for char in name) for name in formatted_cols))
    + "|\n"
)
for person in sorted(report, key=lambda x: x["name"].lower()):
    hr, assignment = person["marks"]["HackerRank"], person["marks"]["Assignment"]
    row = [person["name"], hr, assignment, person["email"], person["hackerrank"]]
    row = list(map(str, row))
    row = [(r + " " * 100)[: col_sizes[c]] for c, r in zip(cols, row)]
    report_text += "| " + " | ".join(row) + "|\n"


with open("../report.md", "w") as fl:
    fl.write(report_text)
