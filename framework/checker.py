import os
import sys
from pathlib import Path

sols = Path("../../framework/solutions")
sys.path.append(str(sols.resolve()))
# ------------------------------
submissions = {}
for pyfile in Path(".").glob("*.py"):
    print(pyfile)
    with open(pyfile, "r") as fl:
        exec(fl.read(), submissions)
# ------------------------------
for fn in submissions.keys():
    if fn.startswith("assignment"):
        print("testing ", fn)
        test = {}
        with open(sols / f"{fn}_solution.py", "r") as fl:
            exec(fl.read(), test)
        try:
            test["test"](submissions[fn])
        except AssertionError as e:
            print(f"Your solution for {fn} fails for input:")
            print(e)
